package th.pichotio.nooknewsbox.app.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import th.pichotio.nooknewsbox.app.database.NewsDBHelper;

/**
 * th.pichotio.nooknewsbox.app.orm
 * Project : NookNewsBox
 * Created by pichotio on 5/20/14 AD.
 */
public class News implements Parcelable {

    private String newsId;
    private String title;
    private String url;
    private String description;

    private List<Button> buttons;

    private String themeColor;
    private String fontColor;

    private Options options;

    public static News newFromJsonObject(JsonObject jsonObject) {
        News news = new News();
        try {
            news.newsId = jsonObject.get("id").getAsString();
        } catch (Exception ignored) {}

        try {
            news.title = jsonObject.get("title").getAsString();
        } catch (Exception ignored) {}

        try {
            news.url = jsonObject.get("url").getAsString();
        } catch (Exception ignored) {}

        try {
            news.description = jsonObject.get("description").getAsString();
        } catch (Exception ignored) {}

        try {
            news.buttons = Button.listFromJsonArray( jsonObject.get("button").getAsJsonArray() );
        } catch (Exception ignored) {}

        try {
            news.themeColor = jsonObject.get("theme_color").getAsString();
        } catch (Exception ignored) {}

        try {
            news.fontColor = jsonObject.get("font_color").getAsString();
        } catch (Exception ignored) {}

        try {
            news.options = Options.newFromJsonObject( jsonObject.get("options").getAsJsonObject() );
        } catch (Exception ignored) {}

        return news;
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(NewsDBHelper.NEWS_ID, this.newsId);
        cv.put(NewsDBHelper.NEWS_TITLE, this.title);
        cv.put(NewsDBHelper.NEWS_START_DATE, this.options.getStartDate());
        cv.put(NewsDBHelper.NEWS_END_DATE, this.options.getEndDate());
        return cv;
    }

    public static News newFromCursor(Cursor cursor) {
        News news = new News();
        try {                                               // Column 0 should be "_id" << default_android
            news.newsId = cursor.getString(1);              // Column 1 should be "id"
            news.title = cursor.getString(2);               // Column 2 should be "title"
            news.options = new Options();
            news.options.setStartDate(cursor.getString(3)); // Column 3 should be "start_date"
            news.options.setEndDate(cursor.getString(4));   // Column 4 should be "end_date"
        } catch (Exception ignored) {}
        return news;
    }

    public String getNewsId() {
        return newsId;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public List<Button> getButtons() {
        List<Button> list = new ArrayList<Button>();
        list.addAll( buttons );
        return list;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public Options getOptions() {
        return Options.clone( options );
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.newsId);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeString(this.description);
        dest.writeTypedList(buttons);
        dest.writeString(this.themeColor);
        dest.writeString(this.fontColor);
        dest.writeParcelable(this.options, 0);
    }

    public News() {
    }

    private News(Parcel in) {
        this.newsId = in.readString();
        this.title = in.readString();
        this.url = in.readString();
        this.description = in.readString();
        in.readTypedList(buttons, Button.CREATOR);
        this.themeColor = in.readString();
        this.fontColor = in.readString();
        this.options = in.readParcelable(((Object) options).getClass().getClassLoader());
    }

    public static Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        public News[] newArray(int size) {
            return new News[size];
        }
    };
}
