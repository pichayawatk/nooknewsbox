package th.pichotio.nooknewsbox.app.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import th.pichotio.nooknewsbox.app.R;
import th.pichotio.nooknewsbox.app.model.News;

/**
 * th.pichotio.nooknewsbox.app.fragment
 * Project : NookNewsBox
 * Created by pichotio on 5/21/14 AD.
 */
public class NewsAlertDialogFragment extends DialogFragment {

    public static NewsAlertDialogFragment newInstance(News news) {
        NewsAlertDialogFragment fragment = new NewsAlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("news", news);
        fragment.setArguments(bundle);
        return fragment;
    }

    @InjectView(R.id.news_alert_titleTextView)  TextView mTitleTextView;

    @OnClick({R.id.news_alert_okBtn, R.id.news_alert_cancelBtn})
    public void willDismissDialog() {
        getDialog().dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_alert_dialog, container, false);

        try {

            News news = getArguments().getParcelable("news");

            mTitleTextView.setText( news.getTitle() );

        } catch (Exception ignored) {}

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
