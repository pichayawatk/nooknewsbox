package th.pichotio.nooknewsbox.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import th.pichotio.nooknewsbox.app.database.NewsDBHelper;
import th.pichotio.nooknewsbox.app.fragment.NewsAlertDialogFragment;
import th.pichotio.nooknewsbox.app.model.News;


public class MainActivity extends Activity {

    private View mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mProgressView = findViewById(R.id.main_progressView);

        // Create new activity
        if(null == savedInstanceState) {

            // Show progress
            mProgressView.setVisibility(View.VISIBLE);

            // Prepare POST params
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("key", "eyJpdiI6IkY4STVZYUROWVVUNXhsM3pQMVlTczNLaVRSaXhvcXF4MGhcL2kzZ2t2RzBvPSIsInZhbHVlIjoiZ0dtOGhnb0dFSjMrMkZMcXFVdmhDSDNURm9EQTc3VlNub1d0RE4zNjM5UT0iLCJtYWMiOiJiZWEyMDI3MjA2YjhhNzNjMWNmN2U5NzIwYTljYjM5ZTRhMGVjMzg2MWViZGYwNjI4YmVjNTg3OTFiMWY0YjdlIn0");
            jsonObject.addProperty("cn", "cn");
            jsonObject.addProperty("cc", "cc");
            jsonObject.addProperty("os", "android");
            jsonObject.addProperty("package", "com.brainsourceapp.app3gx");

            // Request
            Ion.with(this)
                    .load("http://brainsourceapp.com/api/accessControl")
                    .setJsonObjectBody(jsonObject)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject jsonObject) {

                            if(mProgressView != null
                                    && mProgressView.getVisibility() == View.VISIBLE) {

                                mProgressView.setVisibility(View.GONE);
                            } else {

                                // Magic !!
                                return;
                            }

                            ArrayList<News> list = new ArrayList<News>();
                            String errMsg = "";
                            try {

                                JsonObject news = jsonObject.get("news").getAsJsonObject();
                                list.add( News.newFromJsonObject( news ) );

                                afterGetNewsList( list );

                            } catch (Exception e1) {

                                errMsg = ( null != e ) ? e.getLocalizedMessage() : e1.getLocalizedMessage();

                                afterGetErrorMsg( errMsg );
                            }

                        }
                    });
        }
    }

    protected void afterGetNewsList(ArrayList<News> list) {

        // 1. Check news we get from API with "news" in our Database
        NewsDBHelper dbHelper = new NewsDBHelper(getApplicationContext()); // Use application context avoiding memory leak

        for(News news : list) {

            if( dbHelper.canShowNews( news ) ) {

                // HALLELUJAH !!!

                // 1. add to database preventing duplication fault.
                dbHelper.addShowedNews( news );

                // 2. Animate this news ASAP
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.abc_slide_in_top, R.anim.abc_slide_out_top)
                        .replace(R.id.main_fragment_container, NewsAlertDialogFragment.newInstance( news ))
                        .commit();
            }
        }

        // 2. Dont forget to close
        dbHelper.close();
    }

    protected void afterGetErrorMsg(String errMsg) {

        Toast.makeText(getApplicationContext(), errMsg, Toast.LENGTH_LONG).show();
    }
}
