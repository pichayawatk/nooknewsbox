package th.pichotio.nooknewsbox.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * th.pichotio.nooknewsbox.app.orm
 * Project : NookNewsBox
 * Created by pichotio on 5/20/14 AD.
 */
public class Button implements Parcelable {

    private String type;
    private String title;

    public static ArrayList<Button> listFromJsonArray(JsonArray jsonArray) {

        ArrayList<Button> list = new ArrayList<Button>();
        for(int i=0, len=jsonArray.size(); i<len; i++) {

            JsonObject obj = jsonArray.get(i).getAsJsonObject();
            Button button = Button.newFromJsonObject(obj);
            list.add(button);
        }

        return list;
    }

    public static Button newFromJsonObject(JsonObject jsonObject) {
        Button button = new Button();
        try {
            button.type = jsonObject.get("type").getAsString(); } catch (Exception ignored) {}
        try {
            button.title = jsonObject.get("title").getAsString(); } catch (Exception ignored) {}

        return button;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeString(this.title);
    }

    public Button() {
    }

    private Button(Parcel in) {
        this.type = in.readString();
        this.title = in.readString();
    }

    public static Parcelable.Creator<Button> CREATOR = new Parcelable.Creator<Button>() {
        public Button createFromParcel(Parcel source) {
            return new Button(source);
        }

        public Button[] newArray(int size) {
            return new Button[size];
        }
    };
}
