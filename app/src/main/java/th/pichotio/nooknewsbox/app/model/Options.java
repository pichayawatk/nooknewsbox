package th.pichotio.nooknewsbox.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonObject;

/**
 * th.pichotio.nooknewsbox.app.orm
 * Project : NookNewsBox
 * Created by pichotio on 5/20/14 AD.
 */
public class Options implements Parcelable {

    private String key;
    private String startDate;
    private String endDate;

    public static Options clone(Options opt) {
        Options item = new Options();
        item.key = opt.key;
        item.startDate = opt.startDate;
        item.endDate = opt.endDate;
        return item;
    }

    public static Options newFromJsonObject(JsonObject jsonObject) {

        Options item = new Options();
        try {
            item.key = jsonObject.get("key").getAsString(); } catch (Exception ignored) {}
        try {
            item.startDate = jsonObject.get("start_date").getAsString(); } catch (Exception ignored) {}
        try {
            item.endDate = jsonObject.get("end_date").getAsString(); } catch (Exception ignored) {}

        return item;
    }

    public String getKey() {
        return key;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        dest.writeString(this.startDate);
        dest.writeString(this.endDate);
    }

    public Options() {
    }

    private Options(Parcel in) {
        this.key = in.readString();
        this.startDate = in.readString();
        this.endDate = in.readString();
    }

    public static Parcelable.Creator<Options> CREATOR = new Parcelable.Creator<Options>() {
        public Options createFromParcel(Parcel source) {
            return new Options(source);
        }

        public Options[] newArray(int size) {
            return new Options[size];
        }
    };
}
