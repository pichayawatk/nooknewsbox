package th.pichotio.nooknewsbox.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import th.pichotio.nooknewsbox.app.model.News;

/**
 * th.pichotio.nooknewsbox.app.database
 * Project : NookNewsBox
 * Created by pichotio on 5/21/14 AD.
 */
public class NewsDBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;

    private static final String DB_NAME = "newsDB";

    public NewsDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // SQL Statement creating news table
        String CREATE_TABLE_NEWS = "CREATE TABLE news ( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "newsId TEXT, " +
                "title TEXT, " +
                "start_date TEXT, " +
                "end_date TEXT );";

        sqLiteDatabase.execSQL(CREATE_TABLE_NEWS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

        // Drop older if  existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS news");

        // Create new fresh db
        this.onCreate(sqLiteDatabase);
    }

    private static final String TABLE_NEWS = "news";

    public static final String _ID = "_id";
    public static final String NEWS_ID = "id";
    public static final String NEWS_TITLE = "title";
    public static final String NEWS_START_DATE = "start_date";
    public static final String NEWS_END_DATE = "end_date";

    private static final String[] COLUMNS = {_ID, NEWS_ID, NEWS_TITLE, NEWS_START_DATE, NEWS_END_DATE};

    public void addShowedNews(News news) {

        // 1. get reference to write DB.
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues cv = news.toContentValues();

        db.insert(TABLE_NEWS, null, cv);

        db.close();
    }

    public boolean canShowNews(News news) {

        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. build query
        Cursor cursor = db.rawQuery("select * from news where id like ?", new String[] { news.getNewsId() });

        // 3. check that our cursor contain "news"
        if(cursor == null || cursor.isAfterLast()) {
            return true;
        }

        // 4. Set "canShow" flag to TRUE. ( Default )
        boolean canShow = true;

        // 5. Start loop here.
        cursor.moveToFirst();
        while (! cursor.isAfterLast() && canShow) {
            News showedNews = News.newFromCursor(cursor);
            if(showedNews.getNewsId()
                    .equals( news.getNewsId() )) {

                // If there's any news that have the same "newsId" with "news" (method's parameter)
                // it's existed.
                canShow = false;
            }
            cursor.moveToNext();
        }

        // 6. don't forget to close the cursor, thought we get what we want.
        cursor.close();

        return canShow;
    }

}
